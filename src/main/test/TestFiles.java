import com.google.gson.Gson;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.junit.Test;
import superKassa.constant.Constants;
import superKassa.domain.clientInfo.Address;
import superKassa.dto.ratedto.Currencies;
import superKassa.service.files.FileReadWrite;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import java.util.*;

public class TestFiles {

    @Test
    public void readFileAddress() {
        List<String> strings = FileReadWrite.readListFromFile(Constants.ADDRESS_FILE);
        String[] addRtring = strings.get((new Random()).nextInt(strings.size())).split("/");
        Address address = new Address();
        address.setCountry(addRtring[0]);
        address.setCity(addRtring[1]);
        address.setStreet(addRtring[2]);
    }

    @Test
    public void testCurrencies() {
        Client client = ClientBuilder.newClient();
        String response = client.target("http://www.nbrb.by/API/ExRates/Currencies")
                .request(MediaType.APPLICATION_JSON_TYPE.withCharset("utf-8"))
                .accept(MediaType.APPLICATION_JSON_TYPE.withCharset("utf-8")).get(String.class);
        Gson g = new Gson();
        //  Currencies c = g.fromJson(response, Currencies.class);
        List<Currencies> list = Arrays.asList(g.fromJson(response, Currencies[].class));
        System.out.println(list);
        System.out.println(list.size());
    }
}
