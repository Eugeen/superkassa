package superKassa.service;

import com.sun.org.apache.xpath.internal.operations.Bool;
import superKassa.domain.Kassir;
import superKassa.dto.LoginDto;
import superKassa.repository.KassirRepository;

import javax.ejb.Singleton;
import javax.inject.Inject;

@Singleton
public class KassirService {
    @Inject
    private KassirRepository kassirRepository;

    private Kassir currKassir = null;

    public Kassir getCurrKassir() {
        if (currKassir==null){
            currKassir = kassirRepository.getLoginUser("petia","petia");
        }
        return currKassir;
    }

    public Kassir login(LoginDto loginDto) {
        Kassir kassir = kassirRepository.getLoginUser(loginDto.getLogin(), loginDto.getPassword());
        if (kassir == null) {
            return null;
        } else {
            currKassir = kassir;
            return kassir;
        }
    }
}
