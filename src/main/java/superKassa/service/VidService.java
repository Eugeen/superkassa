package superKassa.service;

import superKassa.domain.Contract;
import superKassa.domain.Vid;
import superKassa.repository.ContractRepository;
import superKassa.repository.VidRepository;

import javax.ejb.Singleton;
import javax.inject.Inject;
import java.util.List;

@Singleton
public class VidService {
    @Inject
    private VidRepository vidRepository;

    @Inject
    private ContractRepository contractRepository;

    public List<Vid> vidListAll() {
        return vidRepository.getVidsAll();
    }

    public List<Vid> vidListByRc(String rc) {
        Contract contract = contractRepository.getContractsByRc(rc);
        if (contract == null) {
            return vidListAll();
        }
        return contract.getVids();
    }

}
