package superKassa.service;

import superKassa.domain.Clients;
import superKassa.dto.clientdto.ClientBySelfIdDto;
import superKassa.repository.ClientRepository;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class ClientService {

    @Inject
    private RandomService randomService;

    @Inject
    private ClientRepository clientRepository;

    public Clients saveClientTest(Clients clients) {
        return clientRepository.saveClient(clients);
    }

    public Clients createClients(String fio) {
        Clients client = new Clients();
        client.setFio(fio);
        client.setAddress(randomService.createRandomAddress()); // test
        return client;
    }

    public Clients clientBySelfId(String selfid) {
        Clients client = clientRepository.clientBySelfId(selfid);
        return client;
    }


    public Clients deleteClientBySelfId(String selfid) {
        Clients client = clientRepository.clientBySelfId(selfid);
        if (client!=null) {
            clientRepository.removeClient(client);
        }
        return client;
    }

}
