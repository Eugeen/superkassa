package superKassa.service;


import com.google.gson.Gson;
import superKassa.domain.Currency;
import superKassa.dto.ratedto.Currencies;
import superKassa.dto.ratedto.Rates;
import superKassa.repository.CurrencyRepository;

import javax.ejb.Schedule;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import java.util.*;


@Stateless
public class CurrenciesService {

    @Inject
    private CurrencyRepository currencyRepository;

    private List<Rates> ratesList = new LinkedList<>();

    public List<Currencies> loadCurrencies() {
        Client client = ClientBuilder.newClient();
        String response = client.target("http://www.nbrb.by/API/ExRates/Currencies")
                .request(MediaType.APPLICATION_JSON_TYPE.withCharset("utf-8"))
                .accept(MediaType.APPLICATION_JSON_TYPE.withCharset("utf-8")).get(String.class);
        Gson g = new Gson();
        return Arrays.asList(g.fromJson(response, Currencies[].class));
    }

    public void currenciesToBase() {
        List<Currencies> currencies = loadCurrencies();
        Map<Integer, Currency> currencyList = new HashMap<>();
        for (Currencies c : currencies) {
            currencyList.put(Integer.parseInt(c.getCur_Code()), new Currency(Integer.parseInt(c.getCur_Code()), c.getCur_Abbreviation(), c.getCur_Name(), c.getCur_Name_Bel(), c.getCur_Name_Eng(), null));
        }
        currencyRepository.loadCurrToBase(currencyList);
    }

    public Currency loadCurrById(int id) {
        return currencyRepository.loadCurrById(id);
    }

    public List<Rates> getRates() {
        if (ratesList.size() < 1) {
            loadRates();
        }
        return ratesList;
    }

    // @Schedule(second = "*/10", minute = "*", hour = "*", persistent = false)
    private void loadRates() {
        Client client = ClientBuilder.newClient();
        String response = client.target("http://www.nbrb.by/API/ExRates/Rates?Periodicity=0")
                .request(MediaType.APPLICATION_JSON_TYPE.withCharset("utf-8"))
                .accept(MediaType.APPLICATION_JSON_TYPE.withCharset("utf-8")).get(String.class);
        Gson g = new Gson();
        ratesList = Arrays.asList(g.fromJson(response, Rates[].class));
    }

    public List<Rates> loadRatesByDate(String dateInfo) {
        Client client = ClientBuilder.newClient();
        String url = "http://www.nbrb.by/API/ExRates/Rates?onDate=" + dateInfo + "&Periodicity=0";
        String response = client.target(url)
                .request(MediaType.APPLICATION_JSON_TYPE.withCharset("utf-8"))
                .accept(MediaType.APPLICATION_JSON_TYPE.withCharset("utf-8")).get(String.class);
        Gson g = new Gson();
        return Arrays.asList(g.fromJson(response, Rates[].class));
    }
}
