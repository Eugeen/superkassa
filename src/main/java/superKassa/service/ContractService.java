package superKassa.service;

import superKassa.domain.Contract;
import superKassa.repository.ContractRepository;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.List;

@Singleton
public class ContractService {
    @Inject
    private ContractRepository contractRepository;

    public List<Contract> getContractListAll(){
        return contractRepository.getContractsAll();
    }
}
