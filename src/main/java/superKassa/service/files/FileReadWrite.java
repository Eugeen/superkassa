package superKassa.service.files;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

public class FileReadWrite {

    public static List<String> readListFromFile(String fileName){
        List<String> tmplines = new ArrayList<>();
        try {
            tmplines = Files.readAllLines(Paths.get(fileName), StandardCharsets.UTF_8);
        } catch (IOException e) {
            try {
                tmplines = Files.readAllLines(Paths.get(fileName), Charset.forName("CP1251"));
            } catch (IOException er) {
                System.out.println("Ошибка чтения из файла . . .");
                er.printStackTrace();
            }
        }
        return tmplines;
    }

    public static void writeListToFileV1(List<String> lines, String filename) {
        try (FileWriter writer = new FileWriter(filename, false)) {
            for (String line : lines) {
                writer.write(line + "\n");
            }
            writer.flush();
        } catch (IOException ex) {
            System.out.println("Ошибка! Не удалось записать . . .");
            System.out.println(ex.getMessage());
        }
    }

    public static void writeListToFileV2(List<String> lines, String filename) {
        try  {
            Files.write(Paths.get(filename), lines, StandardCharsets.UTF_8, StandardOpenOption.CREATE);
        } catch (IOException ex) {
            System.out.println("Ошибка! Не удалось записать . . .");
            System.out.println(ex.getMessage());
        }
    }
}
