package superKassa.service;

import superKassa.domain.Kp;
import superKassa.dto.KpMessageInfoDto;

import javax.inject.Singleton;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import java.util.HashMap;
import java.util.Map;

@Singleton
public class SendService {

    public void sendKpInfo(Kp kp) {
        Map<String, String> map = new HashMap<>();
        KpMessageInfoDto kpMessageInfoDto = new KpMessageInfoDto();
        kpMessageInfoDto.setChat_id(441031542);
        kpMessageInfoDto.setText(kpInfoString(kp));
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target("https://api.telegram.org/bot730785332:AAGyWquHK4ESDWHvMK7TjkpwamP25SAvYyo/sendMessage");
        String response = target.request(MediaType.APPLICATION_JSON_TYPE.withCharset("utf-8"))
                .accept(MediaType.APPLICATION_JSON_TYPE.withCharset("utf-8"))
                .post(Entity.json(kpMessageInfoDto), String.class);
    }

    private String kpInfoString(Kp kp) {
        return (kp.isStorno() ? " Storno " : " Create new") +
                " operation:\n"+
                "ID = "+kp.getId()+"\n"+
                "Sum = "+kp.getSum()+"\n"+
                "Comiss = "+kp.getComiss_sum();
    }
}
