package superKassa.service;

import superKassa.domain.Clients;
import superKassa.domain.Currency;
import superKassa.domain.Kp;
import superKassa.domain.Vid;
import superKassa.domain.clientInfo.Passport;
import superKassa.dto.NewKpOperDto;
import superKassa.repository.KpRepository;
import superKassa.repository.VidRepository;

import javax.annotation.PostConstruct;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.inject.Inject;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

@Singleton
//@Startup
public class KpService {

    private LinkedList<Kp> kpList = new LinkedList<>();

    @Inject
    private KassirService kassirService;

    @Inject
    private KpRepository kpRepository;

    @Inject
    private VidRepository vidRepository;

    @Inject
    private SendService sendService;

    @Inject
    private ClientService clientService;

    @Inject CurrenciesService currenciesService;

    @PostConstruct
    private void initKpService() {
        System.out.println("init Kp Service!!!");
    }

    public Kp RegistrKp(NewKpOperDto newKpOperDto) {
        Kp kp = new Kp();
        kp.setDatetime(new Date());
        kp.setStorno(false);
        kp.setSum(newKpOperDto.getSum());
        kp.setKassir(kassirService.getCurrKassir());
        Vid vid = vidRepository.getVidById(newKpOperDto.getVidID());
        kp.setVid(vid);
        Currency currency = currenciesService.loadCurrById(newKpOperDto.getCurr_id());
        kp.setCurrency(currency);
        kp.setComiss_sum(vid.getComissSum());
        kp.setItog(kp.getComiss_sum().add(kp.getSum()));
        Clients clients = clientService.clientBySelfId(newKpOperDto.getSelfid());
        if (clients == null) {
            clients = clientService.createClients(newKpOperDto.getFio());
            Passport passport = new Passport();
            passport.setPassNum(newKpOperDto.getDocnum());
            passport.setPassSer(newKpOperDto.getDocser());
            passport.setSelfId(newKpOperDto.getSelfid());
            clients.setPassport(passport);
            System.out.println(clients);
            clients = clientService.saveClientTest(clients);
        }
        kp.setClients(clients);
        kpList.offer(kp);
        System.out.println(kpList);
//        kpRepository.saveKp(kp);
//        sendService.sendKpInfo(kp);
        return kp;
    }

    @Schedule(second = "*/50", minute = "*", hour = "*", persistent = false)
    private void runKp(){
        if (kpList.size()<1){
            return;
        }
        Kp kp = kpList.pollFirst();
        if (kp == null){
            return;
        }
        kpRepository.saveKp(kp);
        sendService.sendKpInfo(kp);
    }

    public int storKp(int id) {
        Kp kp = kpRepository.getKpByID(id);
        if (kp == null) {
            return -1;
        } else if (kp.isStorno()) {
            return 1;
        }
        kp.setStorno(true);
        kp.setDatetimeStorn(new Date());
        kpRepository.saveKp(kp);
        sendService.sendKpInfo(kp);
        return 0;
    }

    public Kp kpById(int id) {
        return kpRepository.getKpByID(id);
    }

    public List<Kp> kpListAll() {
        return kpRepository.getKpListAll();
    }
}
