package superKassa.service;

import superKassa.domain.clientInfo.Address;
import superKassa.service.files.*;
import superKassa.constant.*;

import javax.ejb.Singleton;
import javax.ejb.Stateless;
import java.util.List;
import java.util.Random;

@Stateless
public class RandomService {
    // test
    public Address createRandomAddress() {
        Address address = new Address();
        List<String> strings = FileReadWrite.readListFromFile(Constants.ADDRESS_FILE);
        String[] addRtring = strings.get((new Random()).nextInt(strings.size())).split("/");
        address.setCountry(addRtring[0]);
        address.setCity(addRtring[1]);
        address.setStreet(addRtring[2]);
        return address;
    }

    public String createRandomFio() {
        List<String> strings = FileReadWrite.readListFromFile(Constants.FIO_FILE);
        return strings.get((new Random()).nextInt(strings.size()));
    }
}
