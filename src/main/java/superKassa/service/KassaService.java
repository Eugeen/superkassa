package superKassa.service;

import superKassa.domain.Kassa;
import superKassa.dto.kassa.KassaInfoDto;
import superKassa.repository.KassaRepository;

import javax.ejb.Singleton;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@Singleton
public class KassaService {
    private List<Kassa> kassaList = new LinkedList<>();

    @Inject
    private KassaRepository kassaRepository;

    private List<Kassa> loadkassaList() {
        kassaList = kassaRepository.loadKassaAll();
        return kassaList;
    }

    public Kassa loadKassaById(int id) {
        return kassaRepository.loadKassaByCurrId(id);
    }

    public List<KassaInfoDto> loadkassaInfoList() {
        List<KassaInfoDto> kassaInfoDtoList = new LinkedList<>();
        kassaList = loadkassaList();
        for (Kassa kassa : kassaList) {
            kassaInfoDtoList.add(new KassaInfoDto(kassa.getCurrency().getId(), kassa.getCurrency().getCur_Name(), kassa.getMoney()));
        }
        return kassaInfoDtoList;
    }
}
