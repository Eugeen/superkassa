package superKassa.dto;

import lombok.Data;

@Data
public class KpMessageInfoDto {
    int chat_id;
    String text;
}
