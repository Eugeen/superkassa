package superKassa.dto.kassa;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class KassaInfoDto {
    private int currId;
    private String currName;
    private BigDecimal money;
}
