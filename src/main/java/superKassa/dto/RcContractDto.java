package superKassa.dto;

import lombok.Data;

@Data
public class RcContractDto {
    private String rc;
}
