package superKassa.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class NewKpOperDto {
    private String fio;
    private String rc;
    private BigDecimal sum;
    private int curr_id;
    private int vidID;
    private String docser;
    private String docnum;
    private String selfid;
}
