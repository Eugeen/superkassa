package superKassa.dto.clientdto;

import lombok.Data;

@Data
public class ClientBySelfIdDto {
    private String selfid;
}
