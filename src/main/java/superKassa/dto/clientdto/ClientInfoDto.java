package superKassa.dto.clientdto;

import lombok.Data;

@Data
public class ClientInfoDto {
    private int id;
    private String fio;
}
