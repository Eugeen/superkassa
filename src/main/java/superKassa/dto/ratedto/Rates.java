package superKassa.dto.ratedto;

import lombok.Data;

import java.util.Date;

@Data
public class Rates {
    private int Cur_ID;
    private String Date;
    private String Cur_Abbreviation;
    private int Cur_Scale;
    private String Cur_Name;
    private String Cur_OfficialRate;
}
