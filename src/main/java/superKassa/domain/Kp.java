package superKassa.domain;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Data
@Table(name = "Kp")
@NamedQueries({
        @NamedQuery(name = "AllKp", query = "select k from Kp k order by k.id"),
        @NamedQuery(name = "operCountByClient", query = "select count(k) from Kp k where k.clients.id = :clientId")
})
public class Kp {
    @Id
    @GeneratedValue
    private int id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "kassir_id")
    private Kassir kassir;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "contract_id")
    private Contract contract;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "vid_id")
    private Vid vid;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "clients_id")
    private Clients clients;

    @Column(name = "sum")
    private BigDecimal sum;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "curr_id")
    private Currency currency;

    @Column(name = "comiss_sum")
    private BigDecimal comiss_sum;

    @Column(name = "date_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date datetime;

    @Column(name = "storno")
    private boolean storno;

    @Column(name = "datetime_storn")
    @Temporal(TemporalType.TIMESTAMP)
    private Date datetimeStorn;

    @Column(name = "itog")
    private BigDecimal itog;

}
