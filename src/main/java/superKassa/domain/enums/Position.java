package superKassa.domain.enums;

public enum Position {
    KASSIR,
    ST_KASSIR,
    ADMIN;
}
