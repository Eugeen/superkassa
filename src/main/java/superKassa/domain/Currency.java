package superKassa.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "Currencies")
public class Currency {

    @Id
    //@GeneratedValue
    private int id;  //Cur_Code

//    @Column(name = "Cur_Code")
//    private int Cur_Code;

    @Column(name = "Cur_Abbreviation")
    private String Cur_Abbreviation;

    @Column(name = "Cur_Name")
    private String Cur_Name;

    @Column(name = "Cur_Name_Bel")
    private String Cur_Name_Bel;

    @Column(name = "Cur_Name_Eng")
    private String Cur_Name_Eng;

    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY,mappedBy = "currency")
    private List<Kassa> kassaList;
}
