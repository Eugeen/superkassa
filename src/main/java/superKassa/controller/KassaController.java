package superKassa.controller;

import superKassa.dto.LoginDto;
import superKassa.dto.NewKpOperDto;
import superKassa.dto.RcContractDto;
import superKassa.dto.clientdto.ClientBySelfIdDto;
import superKassa.mapper.ContractMapper;
import superKassa.mapper.KpMapper;
import superKassa.mapper.UserMapper;
import superKassa.mapper.VidMapper;
import superKassa.service.*;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.text.SimpleDateFormat;
import java.util.Date;


@Path("/kassa")
@Produces("application/json; charset=UTF-8")
public class KassaController {
    @Inject
    private CurrenciesService currenciesService;

    @Inject
    private KassirService userService;

    @Inject
    private KpService kpService;

    @Inject
    private ContractService contractService;

    @Inject
    private VidService vidService;

    @Inject
    private ClientService clientService;

    @Inject
    private CurrenciesService cs;

    @Inject
    private KassaService kassaService;

    @POST
    @Path("/login")
    public Response login(LoginDto loginDto) {
        return Response.ok(UserMapper.kassirToJson(userService.login(loginDto))).build();
    }

    @GET
    @Path("/currkassir")
    public Response currKassir() {
        return Response.ok(userService.getCurrKassir()).build();
    }

    @POST
    @Path("/newkp")
    public Response newKp(NewKpOperDto newKpOperDto) {
        return Response.ok(KpMapper.kpToJson(kpService.RegistrKp(newKpOperDto))).build();
    }

    @POST
    @Path("/storkp/{id}")
    public Response storKp(@PathParam("id") Integer id) {
        switch (kpService.storKp(id)) {
            case 1:
                return Response.status(Response.Status.CONFLICT).entity("Операция уже сторнирована :)").build();
            case -1:
                return Response.status(Response.Status.NOT_FOUND).entity("Операция не найдена :(").build();
            case 0:
                return Response.ok(0).build();
            default:
                return Response.status(600).entity("Всё плохо :(").build();
        }
    }

    @GET
    @Path("/kpById/{id}")
    public Response newKp(@PathParam("id") int id) {
        return Response.ok(KpMapper.kpToJson(kpService.kpById(id))).build();
    }

    @GET
    @Path("/kpall")
    public Response kpAll() {
        return Response.ok(KpMapper.kpListToJson(kpService.kpListAll())).build();
    }

    @GET
    @Path("/vidall")
    public Response vidsAll() {
        return Response.ok(VidMapper.vidListToJson(vidService.vidListAll())).build();
    }

    @POST
    @Path("/vidsbyrc")
    public Response vidsbyrc(RcContractDto rcContractDto) {
        return Response.ok(VidMapper.vidListToJson(vidService.vidListByRc(rcContractDto.getRc()))).build();
    }

    @GET
    @Path("/contractall")
    public Response contractAll() {
        return Response.ok(ContractMapper.contractListToJson(contractService.getContractListAll())).build();
    }

    @POST
    @Path("/clientbyselfid")
    public Response clientBySelfId(ClientBySelfIdDto clientBySelfIdDto) {
        return Response.ok(clientService.clientBySelfId(clientBySelfIdDto.getSelfid())).build();
    }

    @GET
    @Path("/deleteclientbyselfid/{selfid}")
    public Response deleteClientBySelfId(@PathParam("selfid")String selfid) {
        return Response.ok(clientService.deleteClientBySelfId(selfid)).build();
    }

    @GET
    @Path("/loadcurrencies")
    public Response loadCurrencies() {
        return Response.ok(currenciesService.loadCurrencies()).build();
    }

    @GET
    @Path("/loadRatesByDate/{dateStr}")
    public Response loadRatesByDate(@PathParam("dateStr") Date date) {
        String dateStr = new SimpleDateFormat("yyyy-MM-dd").format(date);
        if ((dateStr.equals("")) || (dateStr.equals("Not_Info"))) {
            return Response.ok(currenciesService.getRates()).build();
        }
        return Response.ok(currenciesService.loadRatesByDate(dateStr)).build();
    }

    @GET
    @Path("/loadrates")
    public Response loadRates() {
        return Response.ok(currenciesService.getRates()).build();
    }

    @POST
    @Path("/kassaall")
    public Response loadKassa() {
        return Response.ok(kassaService.loadkassaInfoList()).build();
    }

    @GET
    @Path("/KassaById/{id}")
    public Response loadKassa(@PathParam("id") int id) {
        return Response.ok(kassaService.loadKassaById(id)).build();
    }

    @POST
    @Path("/loadCurrToBase") // Загрузка валют в БД
    public Response loadCurrToBase() {
        cs.currenciesToBase();
        return Response.ok().build();
    }

    @GET
    @Path("/currByCurrID/{curr_id}")
    public Response currByCurrID(@PathParam("curr_id") int curr_id) {
        return Response.ok(currenciesService.loadCurrById(curr_id)).build();

    }
}
