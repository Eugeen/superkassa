package superKassa.repository;

import superKassa.domain.Kp;

import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Singleton
public class KpRepository {

    @PersistenceContext
    private EntityManager em;

    public Kp saveKp(Kp kp) {
        em.getTransaction().begin();
        em.persist(kp);
        em.flush();
        em.getTransaction().commit();
        return kp;
    }

    public Kp getKpByID(int id) {
        return em.find(Kp.class, id);
    }

    public List<Kp> getKpListAll() {
        return em.createNamedQuery("AllKp", Kp.class)
                .getResultList();
    }
}
