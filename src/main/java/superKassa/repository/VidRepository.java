package superKassa.repository;

import superKassa.domain.Vid;

import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Singleton
public class VidRepository {

    @PersistenceContext
    private EntityManager em;

    public List<Vid> getVidsAll() {
        return em.createQuery("select v from Vid v", Vid.class).getResultList();
    }

    public Vid getVidById(int id) {
        return em.find(Vid.class,id);
    }
}
