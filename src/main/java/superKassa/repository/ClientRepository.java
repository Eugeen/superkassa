package superKassa.repository;

import superKassa.domain.Clients;

import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.client.Client;
import java.util.List;

@Singleton
public class ClientRepository {

    @PersistenceContext
    protected EntityManager em;

    public List<Clients> clientsAll() {
        return em.createNamedQuery("clientAll", Clients.class).getResultList();
    }

    public Clients clientById(int id) {
        return em.find(Clients.class, id);
    }

    public Clients saveClient(Clients clients) {
        em.getTransaction().begin();
        em.persist(clients);
        em.flush();
        em.getTransaction().commit();
        return clients;
    }

    public Clients removeClient(Clients client){
        em.getTransaction().begin();
        em.remove(client);
        em.getTransaction().commit();
        return client;
    }

    public Clients clientBySelfId(String selfId) {
        Clients client = null;
        try {
            client = em.createNamedQuery("clientBySelfId", Clients.class)
                    .setParameter("selfId", selfId)
                    .getSingleResult();
        } catch (Exception e) {
            System.out.println("!!!Client Not Found! selfId = " + selfId);
            client = null;
        }
        return client;
    }
}
