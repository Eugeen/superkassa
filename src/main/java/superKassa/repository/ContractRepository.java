package superKassa.repository;

import superKassa.domain.Contract;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Singleton
public class ContractRepository {
    @PersistenceContext
    private EntityManager em;

    public List<Contract> getContractsAll() {
        return em.createQuery("select ct from Contract ct", Contract.class).getResultList();
    }

    public Contract getContractsByRc(String rc) {
        return em.createQuery("select ct from Contract ct where ct.rc=:rc", Contract.class)
                .setParameter("rc", rc)
                .getSingleResult();
    }
}
