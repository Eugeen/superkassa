package superKassa.repository;

import superKassa.domain.Kassa;
import superKassa.dto.ratedto.Currencies;

import javax.ejb.Singleton;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;

@Singleton
public class KassaRepository {
    @PersistenceContext
    private EntityManager em;

    public List<Kassa> loadKassaAll() {
        List<Kassa> kassaList = new ArrayList<>();
        kassaList = em.createQuery("select k from Kassa k", Kassa.class).getResultList();
        return kassaList;
    }

    public Kassa loadKassaByCurrId(Integer CurrId) {
        Kassa kassa = em.createQuery("select k from Kassa k where k.currency.id=:CurrId", Kassa.class).setParameter("CurrId",CurrId).getSingleResult();
        return kassa;
    }
}
