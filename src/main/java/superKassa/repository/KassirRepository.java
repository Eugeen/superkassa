package superKassa.repository;

import superKassa.domain.Kassir;

import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Singleton
public class KassirRepository {
    @PersistenceContext
    private EntityManager em;

    public Kassir getLoginUser(String name, String password) {
        Kassir kassir = null;
        try {
            kassir = em.createQuery("select k from Kassir k where k.login=:login and k.password=:password", Kassir.class)
                    .setParameter("password", password)
                    .setParameter("login", name)
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            kassir = null;
        }
        return kassir;
    }
}
