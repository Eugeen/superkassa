package superKassa.repository;

import superKassa.domain.Currency;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class CurrencyRepository {

    @PersistenceContext
    private EntityManager em;

    public void loadCurrToBase(Map<Integer, Currency> currencyList) {
        em.getTransaction().begin();
        for (Map.Entry entry : currencyList.entrySet()) {
            em.persist(entry.getValue());
        }
        em.getTransaction().commit();
    }

    public Currency loadCurrById(int id) {
        return em.find(Currency.class, id);
    }
}
