package superKassa.mapper;

import superKassa.domain.Kp;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import java.util.List;

public class KpMapper {

    public static JsonArray kpListToJson(List<Kp> kpList) {
        JsonArrayBuilder builder = Json.createArrayBuilder();
        for (Kp kp : kpList) {
            builder.add(kpToJson(kp));
        }
        return builder.build();
    }

    public static JsonObject kpToJson(Kp kp) {
        JsonObject jsonObject = Json.createObjectBuilder()
                .add("ID", kp.getId())
                .add("datetime", kp.getDatetime().toString())
                .add("sum", kp.getSum().toString())
                .add("curr_id", kp.getCurrency().getId())
                .add("curr_name", kp.getCurrency().getCur_Name())
                .add("comisssum", kp.getComiss_sum().toString())
                .add("kassir", kp.getKassir().getFio())
                .add("vid", kp.getVid().getName())
                .add("isstorno", kp.isStorno())
                .add("klientfio",kp.getClients().getFio())
                .add("itog",kp.getItog())
                .build();
        return jsonObject;
    }
}
