package superKassa.mapper;

import superKassa.domain.Contract;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import java.util.List;

public class ContractMapper {

    public static JsonArray contractListToJson(List<Contract> contracts){
        JsonArrayBuilder builder = Json.createArrayBuilder();
        for (Contract contract:contracts){
            builder.add(contractToJson(contract));
        }
        return builder.build();
    }

    public static JsonObject contractToJson(Contract contract){
        JsonObject jsonObject = Json.createObjectBuilder()
                .add("rc",contract.getRc())
                .add("unp",contract.getUnp())
                .add("datebegin",contract.getDateBegin().toString())
                .add("dateend",contract.getDateEnd().toString())
                .build();
        return jsonObject;
    }

}
