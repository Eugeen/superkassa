package superKassa.mapper;

import superKassa.domain.Vid;

import javax.inject.Singleton;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import java.util.List;

@Singleton
public class VidMapper {

    public static JsonArray vidListToJson(List<Vid> vids) {
        JsonArrayBuilder builder = Json.createArrayBuilder();
        for (Vid vid : vids) {
            builder.add(vidToJson(vid));
        }
        return builder.build();
    }

    public static JsonObject vidToJson(Vid vid) {
        JsonObject jsonObject = Json.createObjectBuilder()
                .add("id", vid.getId())
                .add("name", vid.getName())
                .add("comission", vid.getComissSum())
                .build();
        return jsonObject;
    }


}
