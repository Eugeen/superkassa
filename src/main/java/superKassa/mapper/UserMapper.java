package superKassa.mapper;

import superKassa.domain.Kassir;
import superKassa.dto.LoginDto;

import javax.json.Json;
import javax.json.JsonObject;

public class UserMapper {
    public static JsonObject kassirToJson(Kassir kassir){
        JsonObject jsonObject = Json.createObjectBuilder()
                .add("login",kassir.getLogin())
                .add("password",kassir.getPassword())
                .add("fio",kassir.getFio())
                .build();
        return jsonObject;
    }
}
